import './App.css';
//AppNavBar importation
import AppNavBar from './components/AppNavBar.js'
import Home from './pages/Home.js'
import {Container} from 'react-bootstrap'
import Courses from './pages/Courses.js'


function App() {
  return (
    // Fragment "<> and </>"
  /*We are mounting our components and to prepare output rendering*/
  <>
    <AppNavBar/>

    <Container>
      <Home/>
      <Courses/>
    </Container>

  </>

  );
}

export default App;
